// // CLIENT send data
// io.emit( emitHook_YouWantToTest, data_YouWantToTest );
//
// // SRV receive data
// io.on( emitHook_YouWantToTest, (data_YouWantToTest) => {
//     const newDataToDisplay = {...data_YouWantToTest, id: 8};   // do things with data
//     // SRV send data
//     io.emit( listenerName_YouWantToTest, newDataToDisplay);
// } );
//
// // CLIENT receive data
// io.on( listenerName_YouWantToTest, (newDataToDisplay) => {
//     // display data in front into listener corresponding to "emitNameFromClient"
// });
var express = require('express'); // Get the module
var app = express(); // Create express by calling the prototype in var express
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

io.emit( emitHook_YouWantToTest, data_YouWantToTest );

// SRV receive data
io.on( emitHook_YouWantToTest, (data_YouWantToTest) => {
    const newDataToDisplay = {...data_YouWantToTest, id: 8};   // do things with data
    // SRV send data
    io.emit( listenerName_YouWantToTest, newDataToDisplay);
} );

// CLIENT receive data
io.on( listenerName_YouWantToTest, (newDataToDisplay) => {
    // display data in front into listener corresponding to "emitNameFromClient"
});



http.listen(port, function () {
    console.log('listening on *:' + port);
});


