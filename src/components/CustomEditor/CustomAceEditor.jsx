import React from 'react';
import { render } from 'react-dom';
import brace from 'brace';
import AceEditor from 'react-ace';

import 'brace/mode/java';
import 'brace/theme/pastel_on_dark';
import PropTypes from "prop-types";

const CustomAceEditor = (props) => {

    return (
        <div className="custom-object-editor">
            <AceEditor
                defaultValue={"{}"}
                value={props.value}
                mode="json"
                theme="pastel_on_dark"
                onChange={props.onChange}
                name="UNIQUE_ID_OF_DIV"
                editorProps={{$blockScrolling: true}}
            />
        </div>
    );
};


export default CustomAceEditor;
