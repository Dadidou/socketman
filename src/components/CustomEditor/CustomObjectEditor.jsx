import React, {useEffect, useCallback} from "react";
import JSONTree from 'react-json-tree';
import {Map} from 'immutable';

const CustomObjectEditor = (props) => {

    return (
        <div className="custom-object-editor">
            <JSONTree data={props.data} onChange={props.onChange}/>
        </div>
    );

};

export default CustomObjectEditor;
