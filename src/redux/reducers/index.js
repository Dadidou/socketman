import { combineReducers } from 'redux';

import requestsReducer from './requestsReducer';
import listenersReducer from './listenersReducer';

const reducers = combineReducers({
    requestsReducer,            // ES6 property shorthand notation: tabsReducer: tabsReducer
    listenersReducer
});

export default reducers;
