import {EMIT_REQUEST, UPDATE_EDITOR_CONTENT, UPDATE_HOOK_INPUT_VALUE} from "../constants/actionTypes";

// When click on [emit] in the emitter panel
export const emitRequest = (emitData, emitHook) => ({
    type: EMIT_REQUEST,
    payload: emitData
});

// When modifiy editor content
export const updateEditorContent = content => ({
    type: UPDATE_EDITOR_CONTENT,
    payload: content
});

// When modifiy hook input
export const updateHookInputValue = value => ({
    type: UPDATE_HOOK_INPUT_VALUE,
    payload: value
});