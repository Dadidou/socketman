import {createStore} from 'redux';
import reducers from '../reducers';             // Using index.js from reducers (combineReducers())


// tabs: [
//   /*{
//     tabid: 1,
//     request: {
//       emitHook: 'request1',
//       emitData: {
//         "id": "21",
//         "name": "Jean-Claude"
//       },
//       listeners: [
//         {
//          lsnrid
//           hook: 'listener1',
//           response: 'response1'
//         },
//         {
//           hook: 'listener2',
//           response: 'response2'
//         },
//         {
//           hook: 'listener3',
//           response: 'response3'
//         }
//       ],
//       emitDate: '26/12/2019'
//     }
//   },
//
//   {
//     tabid: 2,
//     request: {
//       emitHook: 'request2',
//       emitData: {
//         "id": "44",
//         "name": "Nicolas",
//         "age": "2000"
//       },
//       listeners: [
//         {
//           hook: 'listener4',
//           response: 'response4'
//         },
//         {
//           hook: 'listener5',
//           response: 'response5'
//         }
//       ],
//       emitDate: '27/12/2019'
//     }
//   }*/
// ]

// collections: [
//     {
//         collectionId: 1,
//         label: 'collection1',
//         nbRequests: 1,
//         requests: [
//             {
//             emitHook: 'request1',
//             emitData: {
//                 "id": "21",
//                 "name": "Jean-Claude"
//             },
//             listeners: [
//                 {
//                     hook: 'listener1',
//                     response: 'response1'
//                 },
//                 {
//                     hook: 'listener2',
//                     response: 'response2'
//                 },
//                 {
//                     hook: 'listener3',
//                     response: 'response3'
//                 }
//             ],
//             emitDate: '26/12/2019'
//             }
//         ]
//     }
// ]

// history: [
//     historyId: 1,
//     emitDay: 'December 12',
//     nbRequests: 1,
//         requests: [
//             {
//                 emitHook: 'request1',
//                 emitData: {
//                     "id": "21",
//                     "name": "Jean-Claude"
//                 },
//                 listeners: [
//                     {
//                         hook: 'listener1',
//                         response: 'response1'
//                     },
//                     {
//                         hook: 'listener2',
//                         response: 'response2'
//                     },
//                     {
//                         hook: 'listener3',
//                         response: 'response3'
//                     }
//                 ],
//                 emitDate: '26/12/2019'
//             }
//         ]
//      }
// ]

//---> requests: [
//     {
//         eventName: 'request1',
//         emitData: {
//             "id": "21",
//             "name": "Jean-Claude"
//         },
//         listeners: [
//             {
//                 hook: 'listener1',
//                 response: 'response1'
//             },
//             {
//                 hook: 'listener2',
//                 response: 'response2'
//             },
//             {
//                 hook: 'listener3',
//                 response: 'response3'
//             }
//         ],
//         emitDate: '26/12/2019'
//     }
// ]
// IMPT: Need to return on every case

const Store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default Store;