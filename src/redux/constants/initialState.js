export const INITIAL_STATE = {
    requests: [
        {
            _id: 1,
            emitHook: '',
            emitData: {},
            listeners: [],
            emitDate: '',
            editorContent: '',
            isCurrent: true
        }
    ],
    history: [],
    collections: []
};
//
//
// tabs: [
//   {
//     _id: 1
//   }
//   ]
//
// requests: [
//     {
//         _id,
//         tabid,
//         emitHook: 'request1',
//         emitData: {
//             "id": "21",
//             "name": "Jean-Claude",
//             emitDate: '26/12/2019'
//         }
//     }
// ]
//
// listeners: [
//     {
//         requestid,
//         hook: 'listener1',
//         response: 'response1'
//     }
// ]
